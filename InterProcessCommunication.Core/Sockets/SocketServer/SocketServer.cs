﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Configuration;
using InterProcessCommunication.Core.Sockets.SocketComHandler;

namespace InterProcessCommunication.Core.Sockets
{
    public class ClientConnectedEventArgs<T> : EventArgs
    {
        public ISocketComHandler<T> SocketComHandler;
    }
    public class SocketServer<T> : ISocketServer<T>
    {
        private bool isListening;
        public event EventHandler<ClientConnectedEventArgs<T>> OnClientConnected;

        private TcpListener server;

        public ISocketComHandlerFactory ComHandlerFactory { get; set; }

        public SocketServer()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var ipAddr = appSettings["SocketServerAddr"];
            var port = Int32.Parse(appSettings["SocketServerPort"]);
            Init(ipAddr, port);
        }

        public SocketServer(string ipAddr, int port)
        {
            Init(ipAddr, port);
        }

        private void Init(string ipAddr, int port)
        {
            isListening = false;
            try
            {
                IPAddress localAddr = IPAddress.Parse(ipAddr);
                server = new TcpListener(localAddr, port);
            }
            catch (Exception e)
            {
                Console.WriteLine($"[SocketServer] Exception thrown in constructor: {e}");
            }
        }

        public void Listen()
        {
            if (!isListening)
            {
                isListening = true;
                new Thread(new ThreadStart(ThreadProc)).Start();
            }
        }

        private void ThreadProc()
        {
            // Start listening for client requests.
            server.Start();
            // Enter the listening loop.
            while (true)
            {
                try
                {
                    TcpClient client = server.AcceptTcpClient();

                    var handler = ComHandlerFactory?.Create<T>(client);
                    if (handler != null)
                    {
                        handler.Handle();

                        OnClientConnected?.Invoke(this, new ClientConnectedEventArgs<T>()
                        {
                            SocketComHandler = handler
                        });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error occurred while listing for clients: {e}");
                    isListening = false;
                    server.Stop();
                    return;
                }
            }
        }
    }
}
