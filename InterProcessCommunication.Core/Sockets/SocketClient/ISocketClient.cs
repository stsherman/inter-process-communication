﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterProcessCommunication.Core.Sockets
{
    // T is the Type that the messages received from the ISocketComHandler should be deserialized to
    public interface ISocketClient<T>
    {
        event EventHandler<DataReceivedEventArgs<T>> OnDataReceived;
        void Send(object message);
        void Listen();
    }
}
