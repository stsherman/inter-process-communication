﻿using System;
using InterProcessCommunication.Core.Extensions;
using InterProcessCommunication.Core.Sockets;
using InterProcessCommunication.Core.Sockets.SocketComHandler;

namespace InterProcessCommunication.ConsoleApp
{
    [Serializable]
    public class SocketRequest
    {
        public string Id;
        public string Request;
    }

    [Serializable]
    public class SocketResponse
    {
        public string Id;
        public string Status;
        public string Response;
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var server = new SocketServer<byte[]>();
            server.ComHandlerFactory = new SocketComHandlerFactory();
            server.OnClientConnected += Server_OnClientConnected;
            server.Listen();


            var client = new SocketClient<byte[]>();
            client.ComHandlerFactory = new SocketComHandlerFactory();
            client.OnDataReceived += Client_OnDataReceived;
            client.Listen();

            while (true)
                client.Send(Console.ReadLine());
                //client.Send(new SocketRequest()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Request = Console.ReadLine()
                //});
        }

        private static void Server_OnClientConnected(object sender, ClientConnectedEventArgs<byte[]> e)
        {
            Console.WriteLine($"[SERVER] Client has connected");
            e.SocketComHandler.OnDataReceived += SocketComHandler_OnDataReceived;
            e.SocketComHandler.OnDisconnected += SocketComHandler_OnDisconnected;
        }

        private static void SocketComHandler_OnDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine($"[SERVER] Client has disconnected");
        }

        private static void SocketComHandler_OnDataReceived(object sender, DataReceivedEventArgs<byte[]> e)
        {

            if (sender is SocketComHandler<byte[]> socketClientListener)
            {
                //var response = "";
                //var response = new SocketResponse();
                if (e.ErrorMsg != null)
                {

                    Console.WriteLine($"[SERVER] Error occurred: {e.ErrorMsg}");
                    //response.Status = "Error";
                    //response.Response = $"An error occurred while processing your request: {e.ErrorMsg}";
                    //response = $"An error occurred while processing your request: {e.ErrorMsg}";
                }
                //else
                //{
                //    Console.WriteLine($"[SERVER] Recevied {e.Data.Request}");
                //}
                else
                {
                    Console.WriteLine($"[SERVER] Received {e.Data.Length} bytes");
                    //response = $"Hey thanks for sending me: {e.Data.Length} bytes";
                    //response.Id = e.Data.Id;
                    //response.Status = "Success";
                    //response.Response = $"Hey thanks for sending me: {e.Data.Request}";
                }
                //socketClientListener.Send(response);
                socketClientListener.Send(e.Data);
            }

        }

        private static void Client_OnDataReceived(object sender, DataReceivedEventArgs<byte[]> e)
        {
            if (e.ErrorMsg != null)
                Console.WriteLine($"[CLIENT] Error occurred: {e.ErrorMsg}");
            else
                Console.WriteLine($"[CLIENT] Received {e.Data.Length} bytes");
            //Console.WriteLine($"[CLIENT] Received {e.Data.Response}");
        }
    }
}
